//import thu vien express
const express = require('express');
const path = require('path');

const mongoose=require('mongoose');
const port= 8000;
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365")
.then(() =>{
    console.log("Connect mongoDB Successfully");
})
.catch((error) =>{
    console.log(error);
});

const app= new express();

const drinkModel=require('./app/models/drinkModel');
const voucherModel=require('./app/models/voucherModel');
const orderModel=require('./app/models/orderModel');
const userModel=require('./app/models/userModel');


app.get("/",(req,res) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/views/sample.06restAPI.order.pizza365.v2.0.html"))
})

app.listen(port, () => {
    console.log(`App  listening  on port ${port}`);
})