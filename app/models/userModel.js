const mongoose = require("mongoose");

const Schema= mongoose.Schema;

const userSchema= new Schema({
    _id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true,
        unique:true
    },
    address:{
        type:String,
        required: true
    },
    phone:{
        type: String,
        required:true,
        unique:true
    },
    oders:[
        {
        type: mongoose.Types.ObjectId,
        ref:"order"
        }
    ]
});

module.exports=mongoose.model("user",userSchema);